package interfaz;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Iterator;

import parqueadero.Carro;
import parqueadero.Central;

public class Main 
{
	private BufferedReader br;
	private Central central;

	/**
	 * Clase principal de la aplicaci�nn, incializa el mundo.
	 * @throws Exception
	 */
	public Main() throws Exception
	{

		br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("** PLATAFORMA CITY PARKING S.A.S **");
		System.out.println();
		central = new Central ();
		menuInicial();
	}
	
	/**
	 * Menú principal de la aplicación
	 * @throws Exception
	 */
	public void menuInicial() throws Exception
	{
		String mensaje = "Men� principal: \n"
			+ "1. Registrar Cliente \n"
			+ "2. Parquear Siguente Carro En Cola \n"
			+ "3. Atender Cliente Que Sale \n"
	        + "4. Ver Estado Parqueaderos \n"
	        + "5. Ver Carros En Cola \n"
			+ "6. Salir \n\n"
			+ "Opcion: ";

		boolean terminar = false;
		while ( !terminar )
		{
			System.out.print(mensaje);
			int op1 = Integer.parseInt(br.readLine());
			while (op1>6 || op1<1)
			{
				System.out.println("\nERROR: Ingrese una opci�n valida\n");
				System.out.print(mensaje);
				op1 = Integer.parseInt(br.readLine());
			}

			switch(op1)
			{
				case 1: registrarCliente(); break;
				case 2: parquearCarro(); break;
				case 3: salidaCliente(); break;
				case 4: verEstadoParquederos(); break;
				case 5: verCarrosEnCola(); break;
				case 6: terminar = true; System.out.println("\n Terminacion de Servicios. Hasta pronto."); break;
			}
		}

	}

  /**
   * M�todo para registrar a un nuevo cliente
   * @throws Exception
   */
	public void registrarCliente () throws Exception
  {
	  System.out.println("** REGISTRAR CLIENTE **");
	  System.out.println("Ingrese el nombre del conductor");
	  String nombre = br.readLine();
	  System.out.println("Ingrese el n�mero de matr�cula del veh�culo");
	  String matri = br.readLine();
	  System.out.println("Ingrese el color de su veh�culo");
	  String color = br.readLine();
	  central.registrarCliente(color, matri, nombre);
	  System.out.println("Su veh�culo ha sido agregado a la lista de espera");
  }
  /**
   * M�todo para parquear un carro que se encuentra en la cola
   * @throws Exception
   */
  public void parquearCarro() throws Exception
  {
	  System.out.println("** PARQUEAR CARRO EN COLA **");
	  System.out.println(central.parquearCarroEnCola());
  }
 /**
  * M�todo para sacar un carro del parqueadero
  * @throws Exception
  */
  public void salidaCliente () throws Exception
  {
	  System.out.println("** REGISTRAR SALIDA CLIENTE **");
	  System.out.println("Ingrese el n�mero de matr�cula del veh�culo que desea retirar");
	  String matri = br.readLine();
	  System.out.println("El valor a cancelar es de $" + central.atenderClienteSale(matri));  
  }
  /**
   * M�todo que permite visualizar graficaente el estado de los parqueaderos
   * @throws Exception
   */
  public void verEstadoParquederos() throws Exception
  {
	  System.out.println("** ESTADO PARQUEADEROS **");
	  System.out.println("Fila de espera: " + central.darEspera().getSize());
	  System.out.println("Parqueadero 1: " + central.darP1().getSize());
	  System.out.println("Parqueadero 2: " + central.darP2().getSize());
	  System.out.println("Parqueadero 3: " + central.darP3().getSize());
	  System.out.println("Parqueadero 4: " + central.darP4().getSize());
	  System.out.println("Parqueadero 5: " + central.darP5().getSize());
	  System.out.println("Parqueadero 6: " + central.darP6().getSize());
	  System.out.println("Parqueadero 7: " + central.darP7().getSize());
	  System.out.println("Parqueadero 8: " + central.darP8().getSize());
  }
  
  /**
   * M�todo que permite visualizar graficaente el estado de la cola de carros pendientes por parquear
   * @throws Exception
   */
  public void verCarrosEnCola () throws Exception
  {
	  System.out.println("** ESTADO COLA **");
	  System.out.println(central.darEspera().getSize());
  }
  
  /**
	 * Main...
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			new Main();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

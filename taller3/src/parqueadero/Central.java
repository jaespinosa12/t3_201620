package parqueadero;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Stack;


public class Central
{
    /**
     * Cola de carros en espera para ser estacionados
     */
	private Fila<Carro> espera;
	
	/**
	 * Pilas de carros parqueaderos 1, 2, 3 .... 8
	 */
	private Pila<Carro> p1;
	private Pila<Carro> p2;
	private Pila<Carro> p3;
	private Pila<Carro> p4;
	private Pila<Carro> p5;
	private Pila<Carro> p6;
	private Pila<Carro> p7;
	private Pila<Carro> p8;

    /**
     * Pila de carros parqueadero temporal:
     * Aca se estacionan los carros temporalmente cuando se quiere
     * sacar un carro de un parqueadero y no es posible sacarlo con un solo movimiento
     */
	private Pila<Carro> temporal;
    
    /**
     * Inicializa el parqueadero: Los parqueaderos (1,2... 8) el estacionamiento temporal y la cola de carros que esperan para ser estacionados.
     */
    public Central ()
    {
        espera = new Fila();
        p1 = new Pila();
        p2 = new Pila();
        p3 = new Pila();
        p4 = new Pila();
        p5 = new Pila();
        p6 = new Pila();
        p7 = new Pila();
        p8 = new Pila();
        temporal = new Pila();
    }
    
    /**
     * Registra un cliente que quiere ingresar al parqueadero y el vehiculo ingresa a la cola de carros pendientes por parquear
     * @param pColor color del vehiculo
     * @param pMatricula matricula del vehiculo
     * @param pNombreConductor nombre de quien conduce el vehiculo
     */
    public void registrarCliente (String pColor, String pMatricula, String pNombreConductor)
    {
    	Carro nuevo = new Carro(pColor, pMatricula, pNombreConductor);
    	espera.enqueue(nuevo);
    }    
    
    /**
     * Parquea el siguiente carro en la cola de carros por parquear
     * @return matricula del vehiculo parqueado y ubicaci�n
     * @throws Exception
     */
    public String parquearCarroEnCola() throws Exception
    {
    	Carro nuevo = (Carro) espera.dequeue();
    	String mens = parquearCarro(nuevo); 	
    	return "El carro con la matr�cula: " + nuevo.darMatricula() + " fu� ubicado en el " + mens;
    } 
    
    /**
     * Saca del parqueadero el vehiculo de un cliente
     * @param matricula del carro que se quiere sacar
     * @return El monto de dinero que el cliente debe pagar
     * @throws Exception si no encuentra el carro
     */
    public double atenderClienteSale (String matricula) throws Exception
    {
    	return cobrarTarifa(sacarCarro(matricula));
    }
    
  /**
   * Busca un parqueadero co cupo dentro de los 3 existentes y parquea el carro
   * @param aParquear es el carro que se saca de la cola de carros que estan esperando para ser parqueados
   * @return El parqueadero en el que qued� el carro
   * @throws Exception
   */
    public String parquearCarro(Carro aParquear) throws Exception
    {
    	ArrayList<Pila> temp = new ArrayList<Pila>();
    	temp.add(p1);
    	temp.add(p2);
    	temp.add(p3);
    	temp.add(p4);
    	temp.add(p5);
    	temp.add(p6);
    	temp.add(p7);
    	temp.add(p8);
    	
    	boolean parq = false;
    	String mens = "";
    	
    	for( int i = 0; i < temp.size() && !parq; i++)
    	{
    		Pila actual = temp.get(i);
    		if( actual.getSize() < 4)
    		{
    			actual.push( aParquear );
    			mens = actual.toString();
    			parq = true;
    		}
    	}  
    	return mens;
    }
    
    /**
     * Itera sobre los tres parqueaderos buscando uno con la placa ingresada
     * @param matricula del vehiculo que se quiere sacar
     * @return el carro buscado
     */
    public Carro sacarCarro (String matricula)
    {
    	Carro resp = null;
    	ArrayList<Pila> temp = new ArrayList<Pila>();
    	temp.add(p1);
    	temp.add(p2);
    	temp.add(p3);
    	temp.add(p4);
    	temp.add(p5);
    	temp.add(p6);
    	temp.add(p7);
    	temp.add(p8);
    	for( int i = 0; i < temp.size() && resp == null; i++)
    	{
    		Pila actual = temp.get(i);
    		if( actual == p1 )
    		{
    			resp = sacarCarroP1(matricula);
    		}
    		else if( actual == p2 )
    		{
    			resp = sacarCarroP2(matricula);
    		}
    		else if( actual == p3 )
    		{
    			resp = sacarCarroP3(matricula);
    		}
    		else if( actual == p4 )
    		{
    			resp = sacarCarroP4(matricula);
    		}
    		else if( actual == p5 )
    		{
    			resp = sacarCarroP5(matricula);
    		}
    		else if( actual == p6 )
    		{
    			resp = sacarCarroP6(matricula);
    		}
    		else if( actual == p7 )
    		{
    			resp = sacarCarroP7(matricula);
    		}
    		else if( actual == p8 )
    		{
    			resp = sacarCarroP8(matricula);
    		}
    	}
    	return resp;
    }
    	
    /**
     * Saca un carro del parqueadero 1 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP1 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p1.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p1.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p1.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p1.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p1.push(temporal.pop());
    				p1.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p1.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p1.push(temporal.pop());
        				p1.push(temporal.pop());
        				p1.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    
    /**
     * Saca un carro del parqueadero 2 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP2 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p2.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p2.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p2.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p2.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p2.push(temporal.pop());
    				p2.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p2.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p2.push(temporal.pop());
        				p2.push(temporal.pop());
        				p2.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    
    /**
     * Saca un carro del parqueadero 3 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP3 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p3.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p3.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p3.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p3.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p3.push(temporal.pop());
    				p3.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p3.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p3.push(temporal.pop());
        				p3.push(temporal.pop());
        				p3.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Saca un carro del parqueadero 4 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP4 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p4.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p4.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p4.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p4.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p4.push(temporal.pop());
    				p4.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p4.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p4.push(temporal.pop());
        				p4.push(temporal.pop());
        				p4.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Saca un carro del parqueadero 5 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP5 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p5.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p5.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p5.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p5.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p5.push(temporal.pop());
    				p5.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p5.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p5.push(temporal.pop());
        				p5.push(temporal.pop());
        				p5.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Saca un carro del parqueadero 6 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP6 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p6.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p6.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p6.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p6.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p6.push(temporal.pop());
    				p6.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p6.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p6.push(temporal.pop());
        				p6.push(temporal.pop());
        				p6.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Saca un carro del parqueadero 7 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP7 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p7.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p7.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p7.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p7.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p7.push(temporal.pop());
    				p7.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p7.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p7.push(temporal.pop());
        				p7.push(temporal.pop());
        				p7.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Saca un carro del parqueadero 8 dada su matricula
     * @param matricula del vehiculo buscado
     * @return carro buscado
     */
    public Carro sacarCarroP8 (String matricula)
    {
    	Carro resp = null;
    	Carro temp1 = (Carro) p8.pop();
    	if( temp1.darMatricula().equals(matricula) && temp1 != null)
    	{
    		resp = temp1;
    	}
    	else
    	{
    		temporal.push( temp1 );
    		Carro temp2 = (Carro)p8.pop();
    		if( temp2.darMatricula().equals(matricula) && temp2 != null)
    		{
    			resp = temp2;
    			p8.push(temporal.pop());
    		}
    		else
    		{
    			temporal.push( temp2 );
    			Carro temp3 = (Carro)p8.pop();
    			if( temp3.darMatricula().equals(matricula) && temp3 != null)
    			{
    				resp = temp3;
    				p8.push(temporal.pop());
    				p8.push(temporal.pop());
    			}
    			else
    			{
    				temporal.push( temp3 );
        			Carro temp4 = (Carro)p8.pop();
        			if( temp4.darMatricula().equals(matricula) && temp4 != null)
        			{
        				resp = temp4;
        				p8.push(temporal.pop());
        				p8.push(temporal.pop());
        				p8.push(temporal.pop());
        			}
    			}
    		}
    	}
    	return resp;
    }
    /**
     * Calcula el valor que debe ser cobrado al cliente en funci�n del tiempo que dur� un carro en el parqueadero
     * la tarifa es de $25 por minuto
     * @param car recibe como parametro el carro que sale del parqueadero
     * @return el valor que debe ser cobrado al cliente 
     */
    public double cobrarTarifa (Carro car)
    {
    	Date ahora = new Date();
    	double mins = ahora.getMinutes();
    	double llegada = (car.darLlegada())*60000;
    	return (mins - llegada)*25;	
    }
    
    public String toString( Pila pArqueadero)
    {
    	if( pArqueadero.equals(p1))
    	{
    		return "parqueadero 1";
    	}
    	else if( pArqueadero.equals(p2))
    	{
    		return "parqueadero 2";
    	}
    	else if( pArqueadero.equals(p3))
    	{
    		return "parqueadero 3";
    	}
    	else if( pArqueadero.equals(p4))
    	{
    		return "parqueadero 4";
    	}
    	else if( pArqueadero.equals(p5))
    	{
    		return "parqueadero 5";
    	}
    	else if( pArqueadero.equals(p6))
    	{
    		return "parqueadero 6";
    	}
    	else if( pArqueadero.equals(p7))
    	{
    		return "parqueadero 7";
    	}
    	else if( pArqueadero.equals(p8))
    	{
    		return "parqueadero 8";
    	}
    	else
    		return null;
    }
    
    public Fila darEspera()
    {
    	return espera;
    }
    public Pila darP1()
    {
    	return p1;
    }
    public Pila darP2()
    {
    	return p2;
    }
    public Pila darP3()
    {
    	return p3;
    }
    public Pila darP4()
    {
    	return p4;
    }
    public Pila darP5()
    {
    	return p5;
    }
    public Pila darP6()
    {
    	return p6;
    }
    public Pila darP7()
    {
    	return p7;
    }
    public Pila darP8()
    {
    	return p8;
    }
}
